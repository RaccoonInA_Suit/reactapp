import React, {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import PostStatusFilter from '../post-status-filter';
import PostList from '../post-list';
import PostAddForm from '../post-add-form';
import './app.css';
import styled from 'styled-components';


const AppBlock = styled.div`
    margin: 0 auto;
    max-width: 800px;
`
// const StyledAppBlock = styled(AppBlock)`
//     background-color: grey;
// `

export default class App extends Component {

    state = {
        data: [
    {label: 'Going to learn React', important: true, like: false, id: 1},
    {label: 'That is so good', important: false, like: false, id: 2},
    {label: 'I need a break...', important: false, like: false, id: 3}
        ],
        term: '',
        filter: 'all'
    }
    constructor(props) {
        super(props);
        this.maxId = 4;
        this.onToggleImportant = this.onToggleImportant.bind(this);
        this.onToggleLiked = this.onToggleLiked.bind(this);
        this.onUpdateSearch = this.onUpdateSearch.bind(this);
        this.onFilterSelect = this.onFilterSelect.bind(this);
    }

    

    deleteItem = (id) => {
        this.setState(state => ({
            data: state.data.filter(item => item.id !== id)
        }))
    }

    addItem = (body) => {
        const newItem = {
            label: body,
            important: false,
            id: this.maxId++
        };
        this.setState(state => ({
            data: state.data = [...state.data, newItem]
        }))
    };

    onToggleImportant = (id) => {
        this.setState(({data}) => {
            const index = data.findIndex(elem => elem.id === id);

            const old = data[index];
            const newItem = {...old, important: !old.important};

            const newArr = [...data.slice(0, index), newItem, ...data.slice(index + 1)];
            return {
                data: newArr
            };
        }); 
    };

    onToggleLiked(id) {
        this.setState(({data}) => {
            const index = data.findIndex(elem => elem.id === id);

            const old = data[index];
            const newItem = {...old, like: !old.like};

            const newArr = [...data.slice(0, index), newItem, ...data.slice(index + 1)];
            return {
                data: newArr
            };
        }); 
    }

    searchPost(items, term) {
        if (term.length === 0) {
            return items;
        }

        return items.filter((items) => {
            return items.label.indexOf(term) > - 1;
        }) ;
    } 

    filterPost(items, filter) {
        if (filter === 'like') {
            return items.filter(item => item.like);
        } else {
            return items;
        }
    }

    onUpdateSearch(term) {
        this.setState({term});
    }

    onFilterSelect(filter) {
        this.setState({filter});
    }

    render() {
        const {data,term,filter} = this.state;
        const liked = data.filter(item => item.like).length;
        const allPost = data.length;

        const visiblePost = this.filterPost(this.searchPost(data, term), filter);

        return (
            <AppBlock>
                <AppHeader
                    liked = {liked}
                    allPost = {allPost}/>
                <div className="search-panel d-flex">
                    <SearchPanel
                        onUpdateSearch = {this.onUpdateSearch}
                    />
                    <PostStatusFilter
                        filter = {filter}
                        onFilterSelect = {this.onFilterSelect}
                    />
                </div>
                <PostList   posts={visiblePost} 
                            onDelete = {this.deleteItem}
                            onToggleImportant = {this.onToggleImportant}
                            onToggleLiked = {this.onToggleLiked}
                            />
                <PostAddForm onAdd={this.addItem}/>
            </AppBlock>
            )
    }
}